import json
import numpy as np
import pandas as pd
from keras.models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error  # 均方误差

# 读取验证集csv
X_valid = pd.read_csv("./database/X_valid.csv", index_col=0)
y_valid = pd.read_csv("./database/y_valid.csv", index_col=0)
# 将验证集转为三维，用于模型预测输入
X_valid_3d = np.expand_dims(X_valid, axis=1)
print("X_valid.shape={},y_valid.shape={},X_valid_3d.shape={}".format(X_valid.shape, y_valid.shape, X_valid_3d.shape))

# 载入模型
model = load_model("./result/model.h5")
# 使用经过处理验证集数据，验证模型，并得到预测数据集
pred_valid = model.predict(X_valid_3d)
print(pred_valid.shape)
print(y_valid.shape)

# 获取json文件中的缩放特征: 均值和标准差
filename = './database/scaled_features.json'
with open(filename) as file_obj:
    scaled_features = json.load(file_obj)
print("scaled_features['cnt'][0]={}, scaled_features['cnt'][1]={}".format(
    scaled_features['cnt'][0], scaled_features['cnt'][1]))
mean = scaled_features['cnt'][0]
std = scaled_features['cnt'][1]

pred = []
pred_acc = []
for i in range(len(pred_valid)):
    pred.append(pred_valid[i][0][0] * std + mean)
    pred_acc.append(pred_valid[i][0][0])
# 测试数据的准确性，计算均方误差,及最终的预测精度
cnt = []
for i in y_valid['cnt']:
    cnt.append(i)
mse = mean_squared_error(cnt, pred_acc)
print("Predict accuracy：%.2f%%" % ((1 - mse) * 100))

# 真实值与预测值比较
plt.plot(range(len(pred)), pred, ls='-', lw=2, c='orange', label='Predictive value')
plt.plot(range(len(y_valid['cnt'])), y_valid['cnt'] * std + mean, ls='-.', lw=2, c='blue', label='Actual value')
# 绘制网格
plt.grid(alpha=0.4, linestyle=':')
plt.legend()
plt.xlabel("Verify the data")  # 验证数据
plt.ylabel("Number of riders")  # 骑行人数
plt.savefig("./result/预测值与真实值对比图.png")
plt.show()
