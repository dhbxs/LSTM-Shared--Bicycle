# 第一周作业

## 项目介绍

课题一：基于长短期记忆网络模型（LSTM）的共享单车使用情况预测
共享单车的目的是解决最后1千米问题，国内外提供共享单车服务的公司都非常的多，如何更高效的调度共享单车是交通信息研究的课题，如在某些地铁站出口商业街道旁或某些写字楼下，有时候很难找到共享单车，有时却单车堆积如山，假设你是共享单车服务提供商的数据分析师，如何高效的配置单车资源、如何高效的分析骑行信息？
本课题将使用capital bicycle公司提供的开源数据，可视化的分析和应用公司提供的共享单车在所服务的地区的使用情况，数据集文件在附件中，共有17000多条。 本课题的任务：

1. 数据可视化，数据库当中加载数据然后，可视化输出用户使用单车的情况，直观的显示总骑行人数和相关属性相关性的热图，一年里每月平均骑行人数人数图，在四个季度里以小时为单计数单位的每小时平均的骑行人数图，一天每小时平均骑行人数图。
2. 第二预处理数据，进行数据清洗与分割；
3. 基于 LSTM模型预测：处理序列，参数准备，创建LSTM模型训练模型，不同改进LSTM模型的预测对比分析。

#### 项目结构说明
1. 项目中包含4个文件夹data、、database、doc、result，分别存放训练数据集、数据清洗后的数据以及程序产生的临时数据、参考文档以及开发文档注释、和训练结果包括训练生成的模型以及各种数据分析图像。
2. requirements.txt文件保存项目环境所用到的各种第三方包的版本信息，使用 `pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple` 命令即可安装所有环境。
3. visualization.py是所有相关数据可视化的代码，处理数据并将处理后生成的可视化图片保存在result文件夹中。
4. LSTM-model.py是LSTM长短期记忆模型的代码，包括训练前的数据清洗，模型构件以及训练并保存模型。
5. LSTM-predict.py是调用LSTM-model.py生成的模型进行预测，并验证模型的准确率以及均方差。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_new 分支
3. 提交代码
4. 新建 Pull Request

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)